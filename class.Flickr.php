<?php
	class Flickr{

		private static $api_key = '56830d1c008b44918aa91ac6112f85bb';

		static function search($_params){
			$search = isset($_params['search']) ? $_params['search'] : "";
			$page = isset($_params['page']) ? $_params['page'] : 1;
			$per_page = isset($_params['per_page']) ? $_params['per_page'] : 5;

			$opts = array(
				'api_key'	=> self::$api_key,
				'method'	=> 'flickr.photos.search',
				'tags'	=> $search,
				'format'	=> 'json',
				'per_page'	=> $per_page,
				'page'		=> $page,
			);
			$result = self::call(array('opts'=>$opts));

			$result = str_replace("jsonFlickrApi(", "", $result);
			$result = substr($result, 0, -1);

			return json_decode($result);
		}

		static function call($_params){

			$url = isset($_params['url']) ? $_params['url'] : "https://api.flickr.com/services/rest/?";
			$opts = isset($_params['opts']) ? $_params['opts'] : "";

			$encoded_params = array();
			foreach ($opts as $k => $v){
				$encoded_params[] = urlencode($k).'='.urlencode($v);
			}
			$url.= implode('&', $encoded_params);

			$curl_handle=curl_init();
			curl_setopt($curl_handle, CURLOPT_URL,$url);
			curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl_handle, CURLOPT_USERAGENT, 'FlickrTest');
			$rsp = curl_exec($curl_handle);
			curl_close($curl_handle);

			return $rsp;
		}
	}
?>