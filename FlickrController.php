<?php

	require_once "class.Flickr.php";

	class FlickrController{

		private static $has_post = false;
		private static $search_text = "";
		private static $has_result = false;
		private static $showing_image = false;
		private static $result = array();

		static function init(){
			self::$has_post = false;
			if( isset($_POST['search']) && $_POST['search'] != "" ){
				self::$has_post = true;					// Search will run
				self::$search_text = $_POST['search'];	// We'll need this later
				self::search();
			}
			if( isset($_REQUEST['show']) ){
				self::$showing_image = true;
			}
		}
		static function hasPost(){
			if( self::$has_post ){
				return true;
			}
			return false;
		}
		static function hasResult(){
			if( self::$has_result ){
				return true;
			}
			return false;
		}
		static function showingImage(){
			return self::$showing_image;
		}
		static function showURL(){
			$photo_url = 'https://farm'.$_REQUEST['farm'];
			$photo_url.='.staticflickr.com/'.$_REQUEST['server'];
			$photo_url.='/'.$_REQUEST['id'].'_'.$_REQUEST['secret'].'.jpg';
			return $photo_url;
		}
		public static function searchText(){
			return self::$search_text;
		}
		public static function result(){
			return self::$result;
		}
		public static function page(){
			return self::$result->photos->page;
		}
		public static function pages(){
			return self::$result->photos->pages;
		}
		public static function totalPhotos(){
			return self::$result->photos->total;
		}

		public static function search(){
			$params['search'] = $_POST['search'];
			$params['page'] = isset($_POST['page']) ? $_POST['page'] : 1;
			$params['per_page'] = isset($_POST['per_page']) ? $_POST['per_page'] : 5;

			self::$result = Flickr::search($params);
			if( isset(self::$result->photos) ){
				self::$has_result = true;
			}
		}

	}

?>