<?php
	require_once 'flickrController.php';
	FlickrController::init();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<meta name="description" content="" />
		<meta name="author" content="" />
		<title>Flickr Test</title>

		<link href="css/style.css" rel="stylesheet" />
	</head>
	<body>
		<div class"header">
			<h1>Flickr Test</h1>
		</div>
<?php
	if( FlickrController::showingImage() ){
?>
		<div class="image-container">
			<img src="<?php echo FlickrController::showUrl(); ?>"/>
		</div>
<?php
	}else{
		include "components/SearchBox.php";
	}
?>
<?php
	if( FlickrController::hasResult() ){
		include "components/SearchResults.php";
?>
		<script src="js/index.js"></script>
		<script>
			window.onload = function(){
				Index.page = <?php echo FlickrController::page(); ?>;
				Index.pages = <?php echo FlickrController::pages(); ?>;
				Index.init();
			}
		</script>
<?php	
	}else if(FlickrController::hasPost()){	// Tried... just couldn't do it. Tried out best though.
?>
		<div>
			<h3>Oops. Sorry, couldn't connect to Flickr. Try again please.</h3>
		</div>
<?php
	}
?>
	</body>
</html>