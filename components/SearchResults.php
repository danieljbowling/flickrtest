<div class="photo-container">
<?php
	$result = FlickrController::result();
	$current_page = $result->photos->page;

	$page = FlickrController::page();
	$pages = FlickrController::pages();

	$photos = $result->photos->photo;
	foreach($photos as $photo){
		$farm_id = $photo->farm;
		$server_id = $photo->server;
		$photo_id = $photo->id;
		$secret_id = $photo->secret;
		$size = 't';
		$title = $photo->title;
		$photo_url = 'https://farm'.$farm_id.'.staticflickr.com/'.$server_id.'/'.$photo_id.'_'.$secret_id.'_'.$size.'.'.'jpg';

		$my_url = "index.php?show&farm=$farm_id&server=$server_id&id=$photo_id&secret=$secret_id";
?>
		<div class="photo-cell">
			<a href="<?php echo $my_url; ?>" target="_blank">
				<img title="<?php echo $title; ?>" src="<?php echo $photo_url; ?>"/>
			</a>
		</div>
<?php
	}
?>
</div>
<div class="pagination">
	<button id="previous-btn" title="Previous Page"> &lt; Previous </button>
	<input type="text" placeholder="Go to page" title="Go to page" id="page_index" value="<?php echo $current_page; ?>">
	<button id="next-btn" title="Next Page"> Next &gt; </button>
</div>
<div class="result-info">
	<div>Page <?php echo $page; ?> of <?php echo $pages; ?></div>
</div>
