var Index = {
	init: function(){
		var self = this;
		console.log( self.page );
		console.log( self.pages );
		self.triggers();
	}
	,triggers: function(){
		var self = this;

		document.getElementById("page_index").addEventListener("keydown", function(e){
			if( e.keyCode == 13 ){
				document.getElementById("page").value = document.getElementById("page_index").value;
				document.getElementById("search-form").submit();
			}
		});

		document.getElementById("previous-btn").addEventListener("click", function(){
			if( self.page == 1 ){	// Well clearly you are on the first page already...
				alert("Already on the first page");
			}else{
				document.getElementById("page").value = self.page - 1;
				document.getElementById("search-form").submit();
			}
		});
		document.getElementById("next-btn").addEventListener("click", function(){
			if( self.page == self.pages ){	// no where to go up from here...
				alert("Already on the last page");
			}else{
				document.getElementById("page").value = self.page + 1;
				document.getElementById("search-form").submit();
			}
		});

	}
};